import 'package:flutter/material.dart';

abstract class ColorConstants {
  static Color appColor = const Color(0xFF126CD2);
  static Color buttonColor = const Color(0xFF126CD2);
  static Color secondaryButtonColor = const Color(0xFF1A2129);
  static Color accentColor = const Color(0xFFF3C646);
  static Color secondaryAccentColor = const Color(0xFFFFFAED);
  static Color thirdButtonColor = const Color(0xFFFFFFCC02);
  static Color addToCartButton = const Color(0xFFfbc11c);
  static Color buyNowButton = const Color(0xFFfb641b);
  static Color whiteTextColor = Colors.white;
  static Color secTextColor = Color(0xFF319795);
  static Color splashColor = Color(0xFF1E1C44);
  static Color mainColor = Color(0xFF81E6D9);
  static Color subMainColor = Color(0xFF319795);
}
