import 'dart:async';

import 'package:flutter/material.dart';
import 'package:test_task/utils/constants_color.dart';
import 'package:test_task/utils/utils_responsive_ui.dart';

import 'page_home.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  late Screen size;
  late String valueTheme;

  @override
  void initState() {
    super.initState();
    Timer(const Duration(seconds: 1), () {
      navigateFromSplash();
    });
  }

  @override
  Widget build(BuildContext context) {
    size = Screen(MediaQuery.of(context).size);
    return Scaffold(
        body: Center(
      child: Container(
        color: ColorConstants.splashColor,
      ),
    ));
  }

  void navigateFromSplash() {
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const HomePage()));
  }
}
