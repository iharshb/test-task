import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test_task/utils/constants_color.dart';
import 'package:test_task/widgets/responsive_widget.dart';

class SegmentsWidget extends StatefulWidget {
  @override
  _SegmentsWidgetState createState() => _SegmentsWidgetState();

  final List selectionsList;
  final ValueChanged<int> onSelectTab;
  final VoidCallback onTap;
  final int selectedValue;

  const SegmentsWidget(
      {Key? key,
      required this.selectionsList,
      required this.onSelectTab,
      required this.onTap,
      required this.selectedValue})
      : super(key: key);
}

class _SegmentsWidgetState extends State<SegmentsWidget> {
  Map<int, Widget> tabWidget = <int, Widget>{};
  int selectedTab = 0;

  late ThemeData theme;

  @override
  void initState() {
    super.initState();
    setState(() {
      widget.selectionsList.asMap().forEach((index, value) {
        tabWidget.addAll({
          index: Container(
              height: 40,
              width: 150,
              child: Center(
                child: Text(
                  widget.selectionsList[index],
                ),
              ))
        });
      });
    });
  }

  @override
  void didUpdateWidget(SegmentsWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    theme = Theme.of(context);

    return Responsive(
      mobile: mobileSegment(context),
      desktop: CupertinoSegmentedControl<int>(
        borderColor: Colors.grey.shade400,
        selectedColor: ColorConstants.mainColor,
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        children: tabWidget,
        onValueChanged: (int index) {
          setState(() {
            selectedTab = index;
          });
          widget.onSelectTab(index);
        },
        groupValue: widget.selectedValue,
      ),
      tablet: mobileSegment(context),
    );
  }

  Row mobileSegment(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: CupertinoSegmentedControl<int>(
              borderColor: Colors.grey.shade400,
              selectedColor: ColorConstants.mainColor,
              padding: const EdgeInsets.symmetric(horizontal: 2),
              children: tabWidget,
              onValueChanged: (int index) {
                setState(() {
                  selectedTab = index;
                });
                widget.onSelectTab(index);
              },
              groupValue: widget.selectedValue,
            ),
          ),
        )
      ],
    );
  }
}
