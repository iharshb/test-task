import 'package:flutter/material.dart';
import 'package:test_task/widgets/clipper_header.dart';
import 'package:test_task/widgets/responsive_widget.dart';

import 'utils/constants_color.dart';
import 'widgets/widget_segments.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late Size size;
  int selectedPage = 0;

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey.shade50,
        centerTitle: false,
        actions: [
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Text(
              'Login',
              style: Theme.of(context).textTheme.subtitle1!.copyWith(color: ColorConstants.secTextColor),
              textAlign: TextAlign.center,
            ),
          )
        ],
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(20),
          ),
        ),
      ),
      body: Responsive(
        mobile: mobileView(),
        desktop: webView(),
        tablet: webView(),
      ),
    );
  }

  Widget webView() {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ClipPath(
            clipper: WaveClipper(),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 1.5,
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: [Color(0xFFEBF4FF), Color(0xFFE6FFFA)],
                  begin: FractionalOffset(0.0, 0.0),
                  end: FractionalOffset(1.0, 0.0),
                  stops: [0.0, 1.0],
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(28.0),
                        child: Text(
                          "Diene Job\nwebsite",
                          textAlign: TextAlign.left,
                          style: Theme.of(context)
                              .textTheme
                              .headline3!
                              .copyWith(color: Colors.black, fontWeight: FontWeight.w600),
                        ),
                      ),
                      Container(
                        height: 40.0,
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          gradient: LinearGradient(
                            colors: [Color(0xFF319795), Color(0xFF3182CE)],
                            begin: FractionalOffset(0.0, 0.0),
                            end: FractionalOffset(1.0, 0.0),
                            stops: [0.0, 1.0],
                          ),
                        ),
                        child: ElevatedButton(
                          onPressed: () {},
                          style: ElevatedButton.styleFrom(
                              primary: Colors.transparent,
                              shadowColor: Colors.transparent,
                              padding: const EdgeInsets.symmetric(horizontal: 70)),
                          child: const Text('Kostenlis Registrieren'),
                        ),
                      )
                    ],
                  ),
                  const SizedBox(
                    width: 100,
                  ),
                  const CircleAvatar(
                    backgroundColor: Colors.white,
                    maxRadius: 150,
                    backgroundImage: AssetImage("assets/images/img_agreement.png"),
                  )
                ],
              ),
            ),
          ),
          Column(
            children: [
              SegmentsWidget(
                selectionsList: const ['Arbeitnehmer', 'Arbeitgeber', 'Temporärbüro'],
                onSelectTab: (tab) {
                  setState(() {
                    selectedPage = tab;
                  });
                },
                onTap: () {},
                selectedValue: selectedPage,
              )
            ],
          ),
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Text(
                  selectedPage == 0
                      ? "Drei einfache Schritte zu deinem neuen Job"
                      : selectedPage == 1
                          ? "Drei einfache Schritte zu deinem neuen Mitarbeiter"
                          : "Drei einfache Schritte zur Vermittlung neuer Mitarbeiter",
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline5!.copyWith(color: Colors.black),
                ),
              ),
              sectionOneWebView(
                  "assets/images/profile_one.png",
                  "1.",
                  selectedPage == 2
                      ? "Erhalte Vermittlungs- angebot von Arbeitgeber"
                      : selectedPage == 1
                          ? "Erstellen ein Jobinserat"
                          : "Erstellen dein Lebenslauf",
                  isWeb: true),
              sectionTwoWebView(
                  "assets/images/profile_two.png",
                  "2.",
                  selectedPage == 2
                      ? "Erhalte Vermittlungs- angebot von Arbeitgeber"
                      : selectedPage == 1
                          ? "Erstellen ein Jobinserat"
                          : "Erstellen dein Lebenslauf",
                  isWeb: true),
              sectionOneWebView(
                  "assets/images/profile_three.png",
                  "3.",
                  selectedPage == 2
                      ? "Vermittlung nach Provision oder Stundenlohn"
                      : selectedPage == 1
                          ? "Wähle deinen\nneuen Mitarbeiter aus"
                          : "Mit nur einem Klick bewerben",
                  isWeb: true),
            ],
          ),
        ],
      ),
    );
  }

  Widget mobileView() {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 2.0),
            child: Stack(
              children: [
                ClipPath(
                  clipper: WaveClipper(),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 1.5,
                    decoration: const BoxDecoration(
                      gradient: LinearGradient(colors: [Color(0xFFEBF4FF), Color(0xFFE6FFFA)]),
                    ),
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(28.0),
                          child: Text(
                            "Diene Job\n website",
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline3!.copyWith(color: Colors.black),
                          ),
                        ),
                        Image.asset("assets/images/img_agreement.png")
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: SegmentsWidget(
              selectionsList: const ['Arbeitnehmer', 'Arbeitgeber', 'Temporärbüro'],
              onSelectTab: (tab) {
                setState(() {
                  selectedPage = tab;
                });
              },
              onTap: () {},
              selectedValue: selectedPage,
            ),
          ),
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Text(
                  selectedPage == 0
                      ? "Drei einfache Schritte zu deinem neuen Job"
                      : selectedPage == 1
                          ? "Drei einfache Schritte zu deinem neuen Mitarbeiter"
                          : "Drei einfache Schritte zur Vermittlung neuer Mitarbeiter",
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline5!.copyWith(color: Colors.black),
                ),
              ),
              sectionOneView(
                  "assets/images/profile_one.png",
                  "1.",
                  selectedPage == 2
                      ? "Erhalte Vermittlungs- angebot von Arbeitgeber"
                      : selectedPage == 1
                          ? "Erstellen ein Jobinserat"
                          : "Erstellen dein Lebenslauf",
                  isWeb: true),
              sectionTwoView(
                  "assets/images/profile_two.png",
                  "2.",
                  selectedPage == 2
                      ? "Erhalte Vermittlungs- angebot von Arbeitgeber"
                      : selectedPage == 1
                          ? "Erstellen ein Jobinserat"
                          : "Erstellen dein Lebenslauf",
                  isWeb: true),
              sectionThreeView(
                  "assets/images/profile_three.png",
                  "3.",
                  selectedPage == 2
                      ? "Vermittlung nach Provision oder Stundenlohn"
                      : selectedPage == 1
                          ? "Wähle deinen\nneuen Mitarbeiter aus"
                          : "Mit nur einem Klick bewerben",
                  isWeb: true),
            ],
          ),
        ],
      ),
    );
  }

  Widget sectionOneView(String asset, String number, String info, {bool isWeb = false}) {
    return Column(
      children: [
        Image.asset(
          asset,
          width: size.width / 2,
        ),
        Row(
          children: [
            Text(
              number,
              style: Theme.of(context).textTheme.displayLarge!.copyWith(fontWeight: FontWeight.w500),
            ),
            Expanded(
              child: Text(
                info,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.subtitle1,
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget sectionOneWebView(String asset, String number, String info, {bool isWeb = false}) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.all(50),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text(
                number,
                style: Theme.of(context).textTheme.displayLarge!.copyWith(fontWeight: FontWeight.w500),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 20),
                child: Text(
                  info,
                  style: Theme.of(context).textTheme.headline6,
                ),
              ),
              const SizedBox(
                width: 40,
              ),
              Image.asset(
                asset,
                width: size.width / 4,
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget sectionTwoWebView(String asset, String number, String info, {bool isWeb = false}) {
    return ClipPath(
      clipper: WaveClipper(),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height / 2,
        decoration: const BoxDecoration(
          gradient: LinearGradient(colors: [Color(0xFFE6FFFA), Color(0xFFEBF4FF)]),
        ),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(50),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Image.asset(
                    asset,
                    width: size.width / 4,
                  ),
                  const SizedBox(
                    width: 40,
                  ),
                  Text(
                    number,
                    style: Theme.of(context).textTheme.displayLarge!.copyWith(fontWeight: FontWeight.w500),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                    child: Text(
                      info,
                      style: Theme.of(context).textTheme.headline6,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget sectionTwoView(String asset, String number, String info, {bool isWeb = false}) {
    return ClipPath(
      clipper: WaveClipper(),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height / 2,
        decoration: const BoxDecoration(
          gradient: LinearGradient(colors: [Color(0xFFE6FFFA), Color(0xFFEBF4FF)]),
        ),
        child: Column(
          children: [
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 18.0),
                  child: Text(
                    number,
                    style: Theme.of(context).textTheme.displayLarge!.copyWith(fontWeight: FontWeight.w500),
                  ),
                ),
                Expanded(
                  child: Text(
                    info,
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                ),
              ],
            ),
            Image.asset(
              asset,
              width: size.width / 2,
            ),
          ],
        ),
      ),
    );
  }

  Widget sectionThreeView(String asset, String number, String info, {bool isWeb = false}) {
    return Column(
      children: [
        Row(
          children: [
            Padding(
              padding: EdgeInsets.only(left: size.width / 6),
              child: Text(
                number,
                style: Theme.of(context).textTheme.displayLarge!.copyWith(fontWeight: FontWeight.w500),
              ),
            ),
            Expanded(
              child: Text(
                info,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.subtitle1,
              ),
            ),
          ],
        ),
        Image.asset(
          asset,
          width: size.width / 2,
        ),
      ],
    );
  }
}
